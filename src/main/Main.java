package main;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        MagicSquare magicSquare = new MagicSquare();

        int[][] inputMatrix = new int[3][3];
        Scanner userInput = new Scanner(System.in);
        for (int row = 0; row < inputMatrix.length; row++) {
            System.out.print(String.format("enter the elements of the row [%d] (split values with (,) ): ", row));
            String[] s = userInput.nextLine().split(",");
            for (int i = 0; i < s.length; i++) {
                inputMatrix[row][i] = Integer.parseInt(s[i]);
            }
        }
        System.out.println("the Minimum cost to convert this matrix into magic square is :  " + magicSquare.findMinimumCost(inputMatrix));
    }

}
